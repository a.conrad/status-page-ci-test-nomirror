describe('The user can navigate into a incident correctly', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('Correctly and navigates into a valid incident', () => {
    cy.getComponent('[data-qa-selector="incident-list"]')
      .find('[data-qa-selector="incident-link"]')
      .eq(2)
      .click();

    cy.location().should(url => {
      expect(url.hash).to.eq('#/data%2Fincident%2F1.json');
    });

    cy.getComponent('[data-qa-selector="incident-back-button"]').contains(
      'Return to status overview',
    );

    cy.getComponent('[data-qa-selector="incident-title"]').contains(
      'High error rate on gitlab.com web requests',
    );

    cy.getComponent('[data-qa-selector="incident-status"]').contains('closed');

    cy.getComponent('[data-qa-selector="incident-details"]').contains('Problem to solve');
  });
});
