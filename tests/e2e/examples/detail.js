describe('Details page loads successfully with data when navigated to', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('Loads the details with markup data when navigated to', () => {
    cy.getComponent('[data-qa-selector="incident-list"]')
      .find('[data-qa-selector="incident-link"]')
      .eq(2)
      .click();

    cy.location().should(loc => {
      expect(loc.hash).to.eq('#/data%2Fincident%2F1.json');
    });

    cy.getComponent('[data-qa-selector="incident-back-button"]').contains(
      'Return to status overview',
    );

    cy.getComponent('[data-qa-selector="incident-title"]').contains(
      'High error rate on gitlab.com web requests',
    );

    cy.getComponent('[data-qa-selector="incident-status"]').contains('closed');

    cy.getComponent('[data-qa-selector="incident-details"]').contains('Problem to solve');
  });
});

describe('Details page loads unsuccessfully with error message when navigated to with no data', () => {
  beforeEach(() => {
    cy.server();
    cy.route({
      method: 'GET',
      url: '/data/incident/1.json',
      response: [],
      status: 500,
    });
    cy.visit('/');
  });

  it('Loads the error alert when navigated to', () => {
    cy.getComponent('[data-qa-selector="incident-list"]')
      .find('[data-qa-selector="incident"]')
      .should('have.length', 3);

    cy.getComponent('[data-qa-selector="incident-list"]')
      .find('[data-qa-selector="incident-link"]')
      .eq(2)
      .click();

    cy.location().should(loc => {
      expect(loc.hash).to.eq('#/data%2Fincident%2F1.json');
    });

    cy.getComponent('[data-qa-selector="incident-back-button"]').contains(
      'Return to status overview',
    );

    cy.getComponent('[data-qa-selector="incident-detail-error"]').contains(
      "There was an error loading the incident's detail page. Please refresh the page or try again later.",
    );
  });
});
