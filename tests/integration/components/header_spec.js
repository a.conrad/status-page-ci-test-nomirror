import { GlLink, GlNavbar } from '@gitlab/ui';
import { shallowMount } from '@vue/test-utils';
import { GlHead } from '~/components';

describe('Header component', () => {
  let wrapper;

  function mountComponent() {
    wrapper = shallowMount(GlHead, {
      stubs: { GlNavbar, GlLink },
    });
  }

  beforeEach(() => {
    mountComponent();
  });

  afterEach(() => {
    if (wrapper) {
      wrapper.destroy();
    }
  });

  const findNavbar = () => wrapper.findComponent(GlNavbar);
  const findLink = () => wrapper.findComponent(GlLink);

  it('renders the header component with a navbar and link', () => {
    expect(wrapper.element).toMatchSnapshot();
    expect(findNavbar().exists()).toBe(true);
    expect(findLink().exists()).toBe(true);
  });
});
